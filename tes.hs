listsum [] = 0
listsum (a:as) = a + listsum as

listsum2 as = foldr (+) 0 as

succ3 x = x+3
mult2 x = x*2
fun2 x = x-1
all1 x = (succ3 . mult2) x --mult2 dulu baru succ3
all2 x = (mult2 . succ3) x --succ3 dijalankan dulu, baru mult2

--kpk x y = foldl (min) 0 [z|z<- [x..x*y], z'mod'x==0, z'mod'y==0]

data Expr = C Float | Expr :+ Expr | Expr :- Expr | Expr :* Expr | Expr :/ Expr | V String | Let String Expr Expr deriving Show
evaluate (C x) = x
evaluate (e1 :+ e2) = evaluate e1 + evaluate e2
evaluate (e1 :- e2) = evaluate e1 - evaluate e2
evaluate (e1 :* e2) = evaluate e1 * evaluate e2
evaluate (e1 :/ e2) = evaluate e1 / evaluate e2
evaluate (V _) =  evaluate (C 0.0)

newHOF (C x) f (C y) = x `f` y --HOF untuk expr
--newHOF (C 2) (+) (C 3) = 5.0
newHOFF ((C x) :+ (C y)) = foldl (+) 0 [x, y] --Fungsi tanpa rekursif
mapExp f (C x) = C (f x)
mapExp f (V _) = C (f 0.0)
mapExp f (e1 :+ e2) = mapExp f e1 :+ mapExp f e2
mapExp f (e1 :* e2) = mapExp f e1 :* mapExp f e2
mapExp f (e1 :- e2) = mapExp f e1 :- mapExp f e2
mapExp f (e1 :/ e2) = mapExp f e1 :/ mapExp f e2

subst v0 e0 (V v1) = if (v0 == v1) then e0 else (V v1)
subst _ _ (C c) = (C c)
subst v0 e0 (e1 :+ e2) = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2) = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1 :* e2) = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1 :/ e2) = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2)

data Tree a = Leaf a | Branch (Tree a) (Tree a) deriving (Show, Eq, Ord)

mapTree f (Leaf a) = Leaf (f a)
mapTree f (Branch a b) = Branch (mapTree f a) (mapTree f b)

foldTree f (Leaf a) = a
foldTree f (Branch a b) = (foldTree f a) `f` (foldTree f b)

foldmapTree map fold (Leaf a) = map a
foldmapTree map fold (Branch a b) = (foldmapTree map fold a) `fold` (foldmapTree map fold b)